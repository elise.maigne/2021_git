# L'écosystème Git
## Git : fonctionnement, outils, possibilités

### Elise Maigné, Novembre 2021. 

Présentation pour la rencontre ingénieur statisticiens de Toulouse le 16 décembre 2021 :
http://www.thibault.laurent.free.fr/ingestat.html


Lien vers la présentation : https://elisemaigne.pages.mia.inra.fr/2021_git/index.html

**Cette présentation est susceptible d'être ~~modifiée~~ améliorée au cours du temps !** 
